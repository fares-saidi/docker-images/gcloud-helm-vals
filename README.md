# GCloud-Helm-Vals
# GCloud - Helm - Vals

Docker image to be used in the CI pipelines to with GCloud, Helm, Helm Secret, vals and some other common utilities preinstalled

## Versions

These versions have been tested working as expected

```yaml
HELM_VERSION: v3.10.2
VALS_VERSION: 0.28.1
HELM_SECRETS_VERSION: 4.5.1
google/cloud-sdk: latest
```
