FROM google/cloud-sdk:debian_component_based

ARG HELM_VERSION=v3.10.2
ARG VALS_VERSION=0.28.1
ARG HELM_SECRETS_VERSION=4.5.1
ENV HELM_HOME /helm
ENV HELM_SECRETS_BACKEND=vals
ENV DESIRED_VERSION=${HELM_VERSION}

# Dependencies
RUN apt-get update
RUN apt-get -y install jq wget openssl

# Vals
RUN wget -qO- https://github.com/helmfile/vals/releases/download/v${VALS_VERSION}/vals_${VALS_VERSION}_linux_amd64.tar.gz | tar -xzf- vals && mv vals /usr/local/bin/vals
RUN chmod +x /usr/local/bin/vals

# Helm
RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get
RUN chmod 700 get_helm.sh
RUN . /get_helm.sh

# Helm Secrets
RUN helm plugin install https://github.com/jkroepke/helm-secrets --version v${HELM_SECRETS_VERSION}
